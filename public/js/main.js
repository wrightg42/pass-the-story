$(document).ready(() => {
  $("#login-screen").show();
  $("#lobby-screen").hide();
  $("#game-screen").hide();

  const socket = io();
  var lobby = null;
  var username = null;
  var admin =  null;
  var timeoutDuration = null;
  var timesentenceCallback = null;
  var timeout = null;

  // code responding to join lobby button
  $("#join-lobby").click(() => {
    lobby = $("#lobby").val();
    username = $("#username").val();

    // verify inputs were given
    error = false;
    if (lobby === "") {
      error = true;
      $("#lobby-error").text("Enter a lobby name.");
    }
    if (username === "") {
      error = true;
      $("#username-error").text("Enter a username.");
    }
    if (error) {
      return;
    }

    console.log(`connecting to ${lobby} as ${username}`);
    socket.emit("join", { username: username, lobby: lobby }, (resp) => {
      if (resp.status === "ok") {
        console.log("ok");
        $("#login-screen").hide();
        $("#stories").empty();
        $("#user-list").empty();
        $("#lobby-screen").show();
        $("#passes-form").hide();
        $("#lobby-info").text(`Lobby: ${lobby}`);
      } else {
        console.log(`ERROR: ${resp.error}`)
        if (resp.issue == "lobby") {
          $("#lobby-error").text(resp.error);
        } else {
          $("#username-error").text(resp.error);
        }
      }
    });
  });

  // code responding to start game button
  $("#start-game").click(() => {
    socket.emit("start", { passes: $("#passes").val(), timeout: $("#timeout").val() });
  });

  // code responding to leave lobby button
  $("#leave-game").click(() => {
    socket.emit("leave");
    lobby = username = admin = sentenceCallback = null;
    $("#game-screen").hide();
    $("#lobby-screen").hide();
    $("#login-screen").show();
  });

  // code responding to send sentence button
  $("#send-sentence").click(() => {
    // dont send return if no sentence inputted
    var sentence = $("#sentence").val();
    if (sentence === "") {
      return;
    }

    // send sentece, disable more sends
    sentenceCallback(sentence);
    sentenceCallback = null;
    $("#sentence").prop("disabled", true);
  });

  socket.on("players", (players) => {
    console.log(`got player list\n${players}`);
    $("#passes-form").hide();
    $("#user-list").empty();
    players.forEach((player, i) => {
      li = `<li>${player.admin ? "Host: " : ""}${player.username}</li>`
      if (player.admin) {
        $("#user-list").prepend(li);
      } else {
        $("#user-list").append(li);
      }

      if (player.username === username) {
        admin = player.admin;
        if (admin) {
          $("#passes-form").show();
        }
      }
    });
  });

  socket.on("start", (timeout) => {
    console.log("Game starting");
    timeoutDuration = timeout;
    $("#lobby-screen").hide();
    $("#game-screen").show();
  });

  socket.on("story", (data, fn) => {
    // mark round info
    console.log(data);
    $("#round-info").text(`Lobby ${lobby}: Pass ${data.pass + 1}/${data.passes}`);
    $("#last-sentence").val(data.story.last);
    if (data.story.last === "") {
      $("#last-sentence-small-text").text("No previous sentences, you get to start the story!");
    }

    // reset sentence and hide notgot
    $("#sentence").val("");
    $("#sentence").prop("disabled", false);
    $("#notgot-container").hide();

    // set return callback
    sentenceCallback = fn;

    // create timeout
    timeout = new Date;
    timeout.setTime(timeout.getTime() + timeoutDuration * 1000);
    setInterval(() => {
      $("#timer").text(Math.round((timeout - new Date) / 1000) + " Seconds to send...");
    }, 1000);
  });

  socket.on("stop", () => {
    $("#game-screen").hide();
    $("#lobby-screen").show();
  });

  socket.on("stories", (stories) => {
    console.log(`stories:\n${stories}`);
    $("#stories").empty();

    // add each story to accordion
    stories.forEach((story, i) => {
      const html = `\
          <div class=\"card-header\" id=\"card-header-${i + 1}\"> \
            <h5 class=\"mb-0\"> \
              <button class=\"btn btn-link\" type=\"button\" data-toggle=\"collapse\" data-target=\"#card-content-${i + 1}\"> \
                <span id="story-span-${i + 1}">Story ${i + 1} </span>\
              </button> \
            </h5> \
          </div> \
          <div id=\"card-content-${i + 1}\" class=\"collapse${i == 0 ? " show" : ""}\" aria-labelledby=\"#card-header-${i + 1}\" data-parent=\"#stories\"> \
            <div class=\"card-body\"> \
              <p id="story-p-${i + 1}">${story}</p> \
            </div> \
          </div>`;
      $("#stories").append($("<div/>").attr("class", "card").html(html));
      $("#stories");
    });
  });

  socket.on("notgot", (players) => {
    console.log(`notgot player list\n${players}`);
    $("#notgot-list").empty();
    players.forEach((player, i) => {
      li = `<li>${player}</li>`
      $("#notgot-list").append(li);
    });

    if (sentenceCallback === null) {
      $("#notgot-container").show();
    } else {
      $("#notgot-container").hide();
    }
  });
});
