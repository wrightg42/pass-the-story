// app setup
const express = require("express");
const socket = require("socket.io");
const PORT = process.env.PORT || 5000;
const app = express();
const server = app.listen(PORT, () => {
  console.log(`Listening on port ${PORT}`);
  console.log(`http://localhost:${PORT}`);
});
const io = socket(server);

// static files for web page to host
app.use(express.static("public"));

// useful functions for objects
function getItem(name) {
  name = (name).toLowerCase();
  for (var p in this) {
    if (this.hasOwnProperty(p) && name === p.toLowerCase()) {
      return this[p];
    }
  }

  // nothing found, return undefined
  return undefined;
}

function forEach(callback) {
  length = Object.keys(this).length;
  for (var i = 0; i < length; ++i) {
    key = Object.keys(this)[i];
    item = this[key];
    if (this.hasOwnProperty(key)) {
      callback(item, key, i, this);
    }
  }
}

// lobbies and sockets lists
Object.prototype.forEach = forEach;
const lobbies = {};
const sockets = {};
lobbies.find = getItem;
sockets.find = getItem;

// function to make ID's for the new sockets
function makeID() {
  id = undefined;
  do {
    id = '_' + Math.random().toString(36).substr(2, 9);
  } while (sockets.id !== undefined)
  return id;
}

// Object for Stories
const StoryPrototype = {
  // variables
  story: "",
  last: "",
  gotFromLast: undefined,
  received: false,
  flag: true,

  // adds a sentence to the story
  addSentence: function(sentence, from) {
    this.story += sentence + " ";
    this.last = sentence;
    this.gotFromLast = from;
    this.received = true;
    this.flag = false;
  },

  // marks a story as complete for whatever reason
  // this will flag the story to make sure it is used next round
  markDone: function() {
    this.received = true;
    this.flag = true;
  },

  // removes the received flag
  removeReceived: function() {
    this.received = false;
  }
}

// Object for lobby/games
const LobbyPrototype = {
  // variables
  name: "",
  inProgress: false,
  users: {}, // array of UserPrototype
  admin: "",
  passes: 0,
  pass: 0,
  stories: {}, // array of StoryPrototype
  storyPairs: {}, // dictionary of user -> story for this current pass
  timeout: undefined, // timer object for timeouts
  timeoutDuration: 120000, // 2 minute timeout

  // function for logging purposes
  log: function(message) {
    console.log(`${this.name}: ${message}`);
  },

  // function to get a user/story by username
  getUser: function(name) {
    this.users.find = getItem;
    ret = this.users.find(name);
    delete this.users.find;
    return ret;
  },

  getUserStory: function(username) {
    this.storyPairs.find = getItem;
    ret = this.storyPairs.find(username);
    delete this.storyPairs.find;
    return ret;
  },

  // function to get how many users there are
  userCount: function() {
    return Object.keys(this.users).length;
  },

  // function to add a user
  // NOTE: is ran when you call user.connect, forms 2 way connection
  addUser: function(user) {
    this.users[user.username] = user;
    if (this.admin === "") {
      this.admin = user.username;
    }

    this.log(`added user ${user.username}`);
    this.sendPlayerList();
    if (this.stories !== {}) {
      this.sendStories(this.users[user.username]);
    }
  },

  // removes a user from this lobby
  removeUser: function(name) {
    // get user
    user = this.getUser(name);

    if (this.inProgress) {
      // if game is in progress mark the story this user had as complete to avoid issues
      theirStory = this.getUserStory(name);
      if (!theirStory.received) {
        this.log(`marking ${this.admin.username}'s as complete`);
        theirStory.markDone();
      }

      this.checkReceivedAll();
    }

    delete this.users[user.username];
    this.log(`removed user ${user.username}`);

    // remove self if lobby is now empty
    if (this.userCount() == 0) {
      delete lobbies[this.name];
    } else {
      // change admin if the admin left
      if (this.admin === name) {
        this.admin = this.users[Object.keys(this.users)[0]].username;
        this.log(`new admin is ${this.admin.username}`);
      }

      this.sendPlayerList();
    }
  },

  // sends a message with data to the whole lobby
  sendToLobby: function(message, data) {
    this.log(`sending ${message} to lobby with data:\n${data}`);
    this.users.forEach((user) => {
      user.socket.emit(message, data);
    });
  },

  // sends a message with data to a specific user
  sendToUser: function(user, message, data, callback) {
    this.log(`sending ${message} to ${user.username} with data:\n${data}`);
    user.socket.emit(message, data, callback);
  },

  // sends the whole player list to all the users
  sendPlayerList: function() {
    // get player list as an array of username
    var playerList = [];
    this.users.forEach((user) => {
      playerList.push({ username: user.username, admin: user.username === this.admin });
    });

    // send player list
    this.sendToLobby("players", playerList);
  },

  // sends all stories to all or a user
  // if user === undefined, send to all. If user has a value send to that user
  sendStories: function(user) {
    var stories = []
    this.stories.forEach((story) => {
      stories.push(story.story);
    });

    if (user === undefined) {
      this.sendToLobby("stories", stories);
    } else {
      this.sendToUser(user, "stories", stories);
    }
  },

  // starts a game
  runGame: function(passes, timeout) {
    this.setupGame(passes, timeout);
    this.getSentences();
  },

  // sets up the game to begin
  setupGame: function(passes, timeout) {
    // setup variables
    this.inProgress = true;
    this.passes = passes == 0 ? this.userCount() : passes;
    this.timeoutDuration = timeout;
    this.pass = 0;
    this.stories = [];
    for (var i = 0; i < this.userCount(); ++i) {
      this.stories[i] = Object.create(StoryPrototype);
    }

    this.log(`started game with ${this.passes} passes`)
    this.sendToLobby("start", this.timeoutDuration);
  },

  // gets a sentence from each player using ASYNC callbacks
  getSentences: function() {
    // generate user-story pairings
    this.generatePairings();
    this.storyPairs.forEach((story, username) => {
      user = this.getUser(username);
      this.sendToUser(user, "story", { story: { story: story.story, last: story.last }, pass: this.pass, passes: this.passes }, (sentence) => {
        this.log(`got '${sentence}' from ${username}`);
        story.addSentence(sentence, user);
        this.checkReceivedAll();
      });
    });

    this.timeout = setTimeout(this.timerCallback, this.timeoutDuration * 1000, this);
  },

  timerCallback: function(lobby) {
    // get stories we have not received
    remaining = [];
    lobby.storyPairs.forEach((story, username) => {
      if (!story.received) {
        remaining.push(story);
      }
    });

    // flag unfinished stories
    remaining.forEach((story) => {
      story.markDone();
    });

    // run check all received to start next round
    lobby.checkReceivedAll();
  },

  // function to generate a user-story pairing
  generatePairings: function() {
    // get flagged stories
    flagged = [];
    unflagged = [];
    this.stories.forEach((story) => {
      if (story.flag) {
        flagged.push(story);
      } else {
        unflagged.push(story);
      }
    });

    // get users
    users = [];
    this.users.forEach((user) => {
      users.push(user.username);
    });

    // generate pairing randomly
    this.log(`generating pairings...`);
    pairings = {};
    attempts = 0;
    while (users.length) {
      // generate a random number for the story
      r = Math.random();
      index = 0;

      // get the story, priorotising flagged stories first
      story = undefined;
      if (flagged.length > 0) {
        index = Math.floor(r * Math.floor(flagged.length));
        story = flagged[index];
      } else {
        index = Math.floor(r * Math.floor(unflagged.length));
        story = unflagged[index];
      }

      // check this is not from the current user or give up after 5 attemtps
      if (story.gotFromLast === undefined || users[0] !== story.gotFromLast.username || attempts >= 5) {
        // reset attempts and add pairing
        attempts = 0;
        pairings[users[0]] = story;

        // remove both items from arrays
        users.splice(0, 1);
        if (flagged.length > 0) {
          flagged.splice(index, 1);
        } else {
          unflagged.splice(index, 1);
        }
      } else {
        ++attempts;
      }
    }

    // set pairings and flag unused stories
    this.storyPairs = pairings;
    flagged.forEach((story) => {
      story.markDone();
    });
    unflagged.forEach((story) => {
      story.markDone();
    });
  },

  // checks all messages were recevied
  checkReceivedAll: function() {
    remaining = [];
    this.storyPairs.forEach((story, username) => {
      if (!story.received) {
        remaining.push(username);
      }
    });

  if (remaining.length == 0) {
      // received all parts from players, continue to next pass or end game
      this.runNextRound();
    } else {
      // send not recevied list to players
      this.sendToLobby("notgot", remaining);
    }
  },

  // function to start the next round
  runNextRound: function() {
    ++this.pass;
    clearTimeout(this.timeout);
    this.log(`next round, pass ${this.pass + 1}/${this.passes}`);

    // reset stories recevied flag
    this.stories.forEach((story) => {
      story.removeReceived();
    });

    if (this.pass < this.passes) {
      this.getSentences();
    } else {
      this.finishGame();
    }
  },

  // tidies up the game and sends endgame messages to all players
  finishGame: function() {
    this.log(`game ended`);
    this.inProgress = false;

    // send stories to all players
    this.sendToLobby("stop");
    this.sendStories();
    this.sendPlayerList();
  }
}

// Object for users
const UserPrototype = {
  // variables
  username: "",
  socket: undefined,
  lobby: undefined,

  // function to connect to a lobby
  connect: function(lobbyname, username, socket) {
    this.username = username;
    this.socket = socket;
    this.lobby = lobbies.find(lobbyname);
    lobbies[lobbyname].addUser(this)
  }
}

// new socket connected
io.on("connection", (socket) => {
  // give the socket and ID for debugging
  socket.id = makeID();
  sockets[socket.id] = socket;
  console.log(`new socket connection with id: ${socket.id}`);

  // join game signal
  socket.on("join", (data, fn) => {
    console.log(`${data.lobby}: ${data.username} trying to connect...`);

    // get the lobby and check no game in progress
    lob = lobbies.find(data.lobby);
    if (lob !== undefined) {
      if (lob.inProgress) {
        fn({ status: "error", issue: "lobby", error: "Game in progress. Please wait to join next round."});
        console.log(`${lob.name}: game in progress, ${data.username} unable to join`);
        return;
      }
    } else {
      // make new lobby if not found
      lobbies[data.lobby] = Object.create(LobbyPrototype);
      lobbies[data.lobby].name = data.lobby;
      lob = lobbies[data.lobby];
    }

    // check username is free
    if (lob.getUser(data.username) !== undefined) {
      fn({ status: "error", issue: "username", error: "Username already in use."});
      console.log(`${lob.name}: ${data.username} already being used, unable to join`);
      return;
    }

    // ok to join, save data and connect
    fn({ status: "ok" });
    socket.user = Object.create(UserPrototype);
    socket.user.connect(lob.name, data.username, socket);
    console.log(`${socket.user.lobby.name}: ${socket.user.username} connected`);
  });

  // start game signal
  socket.on("start", (startdata) => {
      // verify the user is the "admin"
      if (socket.user.username === socket.user.lobby.admin) {
        socket.user.lobby.runGame(startdata.passes, startdata.timeout);
      }
  });

  // leaving game signal
  socket.on("leave", () => {
    if (socket.user !== undefined) {
      socket.user.lobby.removeUser(socket.user.username);
    }
    delete socket.user;
  });

  // socket disconnected
  socket.on("disconnect", () => {
    // only run remove user if logged into a game
    if (socket.user !== undefined) {
      socket.user.lobby.removeUser(socket.user.username);
    }

    console.log(`socket disconnected with id: ${socket.id}`);
    socket.removeAllListeners();
    delete sockets[socket.id];
    delete socket;
  });
});
